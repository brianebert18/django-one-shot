from django.urls import path
from todos.views import show_list, detail, create, edit, delete, itemscreate, edit_items

urlpatterns = [
    path("items/<int:id>/edit/", edit_items, name="todo_item_update"),
    path("items/create/", itemscreate, name="todo_item_create"),
    path("<int:id>/delete/", delete, name="todo_list_delete"),
    path("<int:id>/edit/", edit, name="todo_list_update"),
    path("create/", create, name="todo_list_create"),
    path("<int:id>/", detail, name="todo_list_detail"),
    path("", show_list, name="todo_list_list")
]
