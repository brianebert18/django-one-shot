from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoItem, TodoList
from django.db.models import Count
from todos.forms import Todolistform, Todoitemsform

# Create your views here.
def show_list(request):
    todo_lists = TodoList.objects.annotate(item_count=Count("items"))

    context = {
        "todo_lists": todo_lists,
        }
    return render(request,"todos/list.html", context)


def detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/detail.html", context)


def create(request):
    if request.method == "POST":
        form = Todolistform(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = Todolistform()
    context = {
        "form" : form,
    }
    return render(request, "todos/create.html", context)

def itemscreate(request):
    if request.method == "POST":
        form = Todoitemsform(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = Todoitemsform()
    context = {
        "form" : form,
    }
    return render(request, "todos/item/create.html", context)

def edit_items(request, id):
    edited_task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = Todoitemsform(request.POST, instance=edited_task)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail",id=id)
    else:
        form=Todoitemsform(instance=edited_task)
    context = {
        "form" : form,
    }
    return render(request, "todos/item/edit.html", context)



def edit(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = Todolistform(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form=Todolistform(instance=todo_list)
    context = {
        "form" : form,
    }
    return render(request, "todos/edit.html", context)


def delete(request, id):
    deleted = TodoList.objects.get(id=id)
    if request.method == "POST":
        deleted.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
