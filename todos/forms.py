from django.forms import ModelForm
from todos.models import TodoList, TodoItem

class Todolistform(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class Todoitemsform(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
